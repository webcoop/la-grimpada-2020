# La Grimpada 2020

# based from reveal.js

## License

MIT licensed

Copyright (C) 2020 Hakim El Hattab, http://hakim.se

# Development

run an http server with:
```
python -m SimpleHTTPServer 8000
```

If you want to change other than the index:

Install dependencies

```
npm install
```

Serve the presentation and monitor source files for changes

```
npm start
```

Open http://localhost:8000 to view your presentation

You can change the port by using `npm start -- --port=8001`
